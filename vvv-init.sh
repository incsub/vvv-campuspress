# Provision CampusPress Stable

# Make a database, if we don't already have one
echo -e "\nCreating database 'campuspress_default' (if it's not already there)"
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS campuspress_default"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON campuspress_default.* TO wp@localhost IDENTIFIED BY 'wp';"
echo -e "\n DB operations done.\n\n"

# Nginx Logs
if [[ ! -d /srv/log/campuspress-default ]]; then
    mkdir -p /srv/log/campuspress-default
fi

touch /srv/log/campuspress-default/error.log
touch /srv/log/campuspress-default/access.log

# Install and configure the latest stable version of CampusPress
if [[ ! -d "/srv/www/campuspress-default" ]]; then

  echo -n "CampusPress username: "
  read cp_username

  echo "Downloading CampusPress Stable, see http://campuspress.com/"
  cd /srv/www/

  curl -u $cp_username -L -O "https://dist.campuspress.com/latest.tar.gz"

  noroot tar -xvf latest.tar.gz

  mv campuspress campuspress-default
  rm latest.tar.gz
  cd /srv/www/campuspress-default

  echo "Configuring CampusPress Stable..."
  noroot wp core config --dbname=campuspress_default --dbuser=wp --dbpass=wp --quiet --extra-php <<PHP
// Match any requests made via xip.io.
if ( isset( \$_SERVER['HTTP_HOST'] ) && preg_match('/^(local.campuspress.)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(.xip.io)\z/', \$_SERVER['HTTP_HOST'] ) ) {
    define( 'WP_HOME', 'http://' . \$_SERVER['HTTP_HOST'] );
    define( 'WP_SITEURL', 'http://' . \$_SERVER['HTTP_HOST'] );
}

define( 'WP_DEBUG', true );
PHP

  echo "Installing CampusPress Stable..."
  noroot wp core install --url=local.campuspress.dev --quiet --title="Local CampusPress Dev" --admin_name=admin --admin_email="admin@local.dev" --admin_password="password"

else

  echo "Updating CampusPress Stable..."
  cd /srv/www/campuspress-default
  noroot wp core update

fi
